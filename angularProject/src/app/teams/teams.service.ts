import { Injectable } from '@angular/core';
import {Team} from "../models/team";
import {Observable, of} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Content} from "@angular/compiler/src/render3/r3_ast";

const isDev : boolean = false;
const API_Team_URI: string = 'https://localhost:5001/api/Teams';
const getId_URI: string = '/id?id=';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  localTeams: Team[] = [];
  constructor(private http: HttpClient) {
    if(isDev) {
      this.localTeams.push(new Team(1, 'McDonalds', new Date()));
    }
  }

  getTeam(id: number) : Observable<Team> {
    if(isDev) {
      return of(this.localTeams[id-1]);
    } else {
      return this.http.get<Team>(API_Team_URI + getId_URI + id);
    }
  }
  getAllTeams() : Observable<Team[]> {
    if(isDev) {
      return of(this.localTeams);
    } else {
      return this.http.get<{id:number, name: string, createdAt: Date}[]>(API_Team_URI).pipe(
        map(t => {
          return t.map(tr => new Team(tr.id, tr.name, new Date(tr.createdAt)));
        })
      );
    }
  }
  updateTeam(id: number, team: Team) : void {
    team.id = id;
    if(isDev) {
      team.createdAt = this.localTeams[id-1].createdAt;
      this.localTeams[id-1] = team;
    }
    else {
      console.log(team.name);
      this.http.put(API_Team_URI, {
        id: team.id,
        name: team.name
      }).subscribe(req => {
        error: (error: Error) => {
          console.error(error.message)
        }
      });
    }
  }
  createTeam(teamName: Team) : void {
    if(isDev) {
      let team: Team = new Team(this.localTeams.length + 1,
        teamName.name, new Date());
      this.localTeams.push(team);
    }
    else {
      let body: string = JSON.stringify({ name: teamName.name})
      let header: HttpHeaders = new HttpHeaders();
      header.append('Accept', 'application/json');
      header.append('Content-Type', 'application/json');
      console.log(body);
      this.http.post(API_Team_URI, { name: teamName.name.toString()},
        {headers: header}).subscribe();
    }
  }

}
