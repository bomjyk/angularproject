import {Component, OnInit, ViewChild} from '@angular/core';
import {TeamsService} from "../teams.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, FormGroup, FormsModule, NgForm, Validators} from "@angular/forms";
import {Project} from "../../models/project";
import {ProjectServiceService} from "../../projects-module/project-service.service";
import {Team} from "../../models/team";

@Component({
  selector: 'app-teams-edit',
  templateUrl: './teams-edit.component.html',
  styleUrls: ['./teams-edit.component.css']
})
export class TeamsEditComponent implements OnInit {
  @ViewChild("saveForm")
  private saveForm : NgForm | null = null;
  formSubmited: boolean = false;
  form!: FormGroup;
  team!: Team;
  teamForm: FormGroup = new FormGroup({
    name: new FormControl('[Name]')
  });
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _service: TeamsService,
    private _forms: FormsModule
  ) { }

  id!: number;

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.id = +params['id'];
      this.getTeam(this.id);
    });
  }

  private getTeam(id: number) {
    this._service.getTeam(id).subscribe(pr => {
      console.log(pr);
      this.team = new Team(pr.id, pr.name, pr.createdAt);
      this.teamForm = new FormGroup({
        name: new FormControl(pr.name, [Validators.required, Validators.minLength(2)]),
      });
    });
  }
  onSubmit() {
    this.formSubmited = true;
    this.updateProject();
  }
  updateProject() {
    console.log(this.teamForm.value);
    this._service.updateTeam(this.id, this.teamForm.value);
    this._service.getAllTeams();
    this._router.navigate(['../../list'], {relativeTo: this._route})
  }
  hasUnsavedData() : boolean {
    return <boolean>this.saveForm?.dirty;
  }
}
