import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TeamsService} from "./teams.service";
import { TeamsListComponent } from './teams-list/teams-list.component';
import {UkrainianDatePipe} from "../pipes/ukrainianDatePipe";
import {RouterModule, Routes} from "@angular/router";
import {Team} from "../models/team";
import { TeamsEditComponent } from './teams-edit/teams-edit.component';
import { TeamsCreateComponent } from './teams-create/teams-create.component';
import {CustomPipesModuleModule} from "../pipes/cutom-pipes-module/cutom-pipes-module.module";
import {ReactiveFormsModule} from "@angular/forms";
import {TeamEditDeactivateGuard} from "../guards/teams/team-edit-deactivate.guard";
import {TeamCreateDeactivateGuard} from "../guards/teams/team-create-deactivate.guard";

const routes: Routes = [
  {path: 'team/list', component: TeamsListComponent},
  {path: 'team/edit/:id', component: TeamsEditComponent,
  canDeactivate: [TeamEditDeactivateGuard]},
  {path: 'team/create', component: TeamsCreateComponent,
  canDeactivate: [TeamCreateDeactivateGuard]}
]

@NgModule({
  declarations: [
    TeamsListComponent,
    TeamsEditComponent,
    TeamsCreateComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    CustomPipesModuleModule,
    ReactiveFormsModule
  ],
  exports: [
    TeamsListComponent
  ],
  providers: [
    TeamsService,
  ]
})
export class TeamsModule { }
