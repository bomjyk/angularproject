import { Component, OnInit, OnDestroy } from '@angular/core';
import {TeamsService} from "../teams.service";
import {Team} from "../../models/team";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-teams-list',
  templateUrl: './teams-list.component.html',
  styleUrls: ['./teams-list.component.css']
})
export class TeamsListComponent implements OnInit, OnDestroy {

  teams: Team[] = [];
  subscription: Subscription = new Subscription();

  constructor(private _service: TeamsService,
              private _router: Router,
              private _activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.subscription = this._service.getAllTeams().subscribe(_teams => {
      this.teams = _teams;
      console.log(this.teams);
    });
  }

  ngOnDestroy() : void {
    //this.subscription.unsubscribe();
  }

  showEditForm(id: number) {
    this._router.navigate(['team/edit', id])
  }

  createNewTeam() {
    this._router.navigate(['team/create']);
  }
}
