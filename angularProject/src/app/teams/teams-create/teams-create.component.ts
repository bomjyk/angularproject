import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Project} from "../../models/project";
import {ProjectServiceService} from "../../projects-module/project-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Team} from "../../models/team";
import {TeamsService} from "../teams.service";

@Component({
  selector: 'app-teams-create',
  templateUrl: './teams-create.component.html',
  styleUrls: ['./teams-create.component.css']
})
export class TeamsCreateComponent implements OnInit {

  @ViewChild("saveForm")
  private saveForm : NgForm | null = null;
  formSubmited: boolean = false;
  team!: Team;
  teamForm: FormGroup = new FormGroup({
    Name: new FormControl('', [Validators.required,
    Validators.minLength(2)])
  });
  constructor(
    private _service: TeamsService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.formSubmited = true;
    this._service.createTeam(this.teamForm.value);
    this._router.navigate(['../list'], { relativeTo: this._route });
  }

  public hasUnsavedData() : boolean {
    return <boolean>this.saveForm?.dirty;
  }
}
