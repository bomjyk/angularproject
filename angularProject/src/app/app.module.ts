import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ProjectsModuleModule } from "./projects-module/projects-module.module";
import { Routes, RouterModule } from "@angular/router";
import {ProjectsListComponent} from "./projects-module/projects-list/projects-list.component";
import {UkrainianDatePipe} from "./pipes/ukrainianDatePipe";
import { TaskHighlightDirective } from './directives/task-highlight.directive';
import {TeamsListComponent} from "./teams/teams-list/teams-list.component";
import {TeamsModule} from "./teams/teams.module";
import {TasksModule} from "./tasks/tasks.module";
import {TasksListComponent} from "./tasks/tasks-list/tasks-list.component";
import {UserListComponent} from "./users/user-list/user-list.component";
import {UsersModule} from "./users/users.module";


const routes : Routes = [
  {path: 'project/list', component: ProjectsListComponent},
  {path: 'team/list', component: TeamsListComponent},
  {path: 'task/list', component: TasksListComponent},
  {path: 'user/list', component: UserListComponent}
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ProjectsModuleModule,
    TeamsModule,
    TasksModule,
    UsersModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
