import {Pipe, PipeTransform } from "@angular/core";

const monthMap : Map<number,string> = new Map<number, string>([
  [0, 'січня'],
  [1, 'лютого'],
  [2, 'березеня'],
  [3, 'квітня'],
  [4, 'травня'],
  [5, 'червня'],
  [6, 'липня'],
  [7, 'серпня'],
  [8, 'вересня'],
  [9, 'жовтня'],
  [10, 'листопада'],
  [11, 'грудня'],
])

@Pipe({name: 'ukrainianDate'})

export class UkrainianDatePipe implements PipeTransform {
  transform(value: Date): string {
    return (value.getDate() + ' '
      + monthMap.get(value.getMonth()) + ' '
      + value.getFullYear());
  }
}
