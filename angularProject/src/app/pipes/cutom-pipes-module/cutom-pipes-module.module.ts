import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UkrainianDatePipe } from "../ukrainianDatePipe";


@NgModule({
  declarations: [
    UkrainianDatePipe
  ],
  imports: [
  ],
  exports: [
    UkrainianDatePipe
  ]
})
export class CustomPipesModuleModule { }
