import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {TeamsEditComponent} from "../../teams/teams-edit/teams-edit.component";

@Injectable({
  providedIn: 'root'
})
export class TeamEditDeactivateGuard implements CanDeactivate<TeamsEditComponent> {
  canDeactivate(
    component: TeamsEditComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData() ||
      confirm("There are unsaved data on the page. Continue?");
  }

}
