import { TestBed } from '@angular/core/testing';

import { TeamEditDeactivateGuard } from './team-edit-deactivate.guard';

describe('TeamEditDeactivateGuard', () => {
  let guard: TeamEditDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TeamEditDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
