import { TestBed } from '@angular/core/testing';

import { TeamCreateDeactivateGuard } from './team-create-deactivate.guard';

describe('TeamCreateDeactivateGuard', () => {
  let guard: TeamCreateDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TeamCreateDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
