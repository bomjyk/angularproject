import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {TeamsCreateComponent} from "../../teams/teams-create/teams-create.component";

@Injectable({
  providedIn: 'root'
})
export class TeamCreateDeactivateGuard implements CanDeactivate<TeamsCreateComponent> {
  canDeactivate(
    component: TeamsCreateComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData() ||
      confirm("There are unsaved data on the page. Continue?");
  }

}
