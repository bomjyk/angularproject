import { TestBed } from '@angular/core/testing';

import { ProjectEditDeactivateGuard } from './project-edit-deactivate.guard';

describe('ProjectEditDeactivateGuard', () => {
  let guard: ProjectEditDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ProjectEditDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
