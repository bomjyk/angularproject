import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {ProjectCreateComponent} from "../projects-module/project-create/project-create.component";

@Injectable({
  providedIn: 'root'
})
export class ProjectFormDeactivateGuard implements CanDeactivate<ProjectCreateComponent> {
  canDeactivate(
    component: ProjectCreateComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData() || confirm("There is unsaved changes on page. Continue?", );
  }

}
