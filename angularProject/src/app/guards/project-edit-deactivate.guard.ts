import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {ProjectsEditComponent} from "../projects-module/projects-edit/projects-edit.component";

@Injectable({
  providedIn: 'root'
})
export class ProjectEditDeactivateGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: ProjectsEditComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData()
      || confirm("There is unsaved changes on page. Continue?");
  }

}
