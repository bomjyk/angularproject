import { TestBed } from '@angular/core/testing';

import { UserCreateDeactivateGuard } from './user-create-deactivate.guard';

describe('UserCreateDeactivateGuard', () => {
  let guard: UserCreateDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UserCreateDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
