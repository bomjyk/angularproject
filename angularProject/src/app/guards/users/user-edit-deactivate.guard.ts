import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {UserEditComponent} from "../../users/user-edit/user-edit.component";

@Injectable({
  providedIn: 'root'
})
export class UserEditDeactivateGuard implements CanDeactivate<UserEditComponent> {
  canDeactivate(
    component: UserEditComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData() ||
      confirm("There are unsaved data on the page. Continue?");
  }

}
