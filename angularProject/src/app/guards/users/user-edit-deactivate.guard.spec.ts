import { TestBed } from '@angular/core/testing';

import { UserEditDeactivateGuard } from './user-edit-deactivate.guard';

describe('UserEditDeactivateGuard', () => {
  let guard: UserEditDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UserEditDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
