import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {UserCreateComponent} from "../../users/user-create/user-create.component";

@Injectable({
  providedIn: 'root'
})
export class UserCreateDeactivateGuard implements CanDeactivate<UserCreateComponent> {
  canDeactivate(
    component: UserCreateComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData() ||
      confirm("There are unsaved data on the page. Continue? ");
  }

}
