import { TestBed } from '@angular/core/testing';

import { ProjectFormDeactivateGuard } from './project-form-deactivate.guard';

describe('ProjectFormDeactivateGuard', () => {
  let guard: ProjectFormDeactivateGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ProjectFormDeactivateGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
