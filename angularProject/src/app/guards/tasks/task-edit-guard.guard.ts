import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {TasksEditComponent} from "../../tasks/tasks-edit/tasks-edit.component";

@Injectable({
  providedIn: 'root'
})
export class TaskEditGuardGuard implements CanDeactivate<TasksEditComponent> {
  canDeactivate(
    component: TasksEditComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData() ||
      confirm("There are unsaved data on the page. Continue? ");
  }

}
