import { TestBed } from '@angular/core/testing';

import { TaskEditGuardGuard } from './task-edit-guard.guard';

describe('TaskEditGuardGuard', () => {
  let guard: TaskEditGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TaskEditGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
