import { TestBed } from '@angular/core/testing';

import { TaskCreateGuardGuard } from './task-create-guard.guard';

describe('TaskCreateGuardGuard', () => {
  let guard: TaskCreateGuardGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TaskCreateGuardGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
