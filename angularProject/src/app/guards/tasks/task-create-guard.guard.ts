import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {TasksCreateComponent} from "../../tasks/tasks-create/tasks-create.component";

@Injectable({
  providedIn: 'root'
})
export class TaskCreateGuardGuard implements CanDeactivate<TasksCreateComponent> {
  canDeactivate(
    component: TasksCreateComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return component.formSubmited || !component.hasUnsavedData() ||
      confirm("There is unsaved data on the page. Continue?");
  }

}
