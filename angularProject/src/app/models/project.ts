import {User} from "./User";
import {Team} from "./team";

export class Project {
  id: number;
  author: User;
  team: Team;
  name: string;
  description: string;
  deadline: Date;
  createdAt: Date;
  constructor(Id:number,
              Author: User,
              Team: Team,
              Name: string,
              Description: string,
              Deadline: Date,
              CreatedAt: Date) {
    this.id = Id;
    this.author = Author;
    this.team = Team;
    this.name = Name;
    this.description = Description;
    this.deadline = Deadline;
    this.createdAt = CreatedAt;
  }
}
