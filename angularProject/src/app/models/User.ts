import {Team} from "./team";


export class User {
  Id: number;
  Name: string;
  Email: string;
  Team: Team;
  Birthday: Date;
  RegisteredAt: Date;

  public constructor(
    id: number, name: string,
    email: string, team: Team,
    birthday: Date, registeredAt: Date
  ) {
    this.Id = id;
    this.Name = name;
    this.Email = email;
    this.Team = team;
    this.Birthday = birthday;
    this.RegisteredAt = registeredAt;
  }
}
