export class UserDTO {
  id: number;
  teamId!:number;
  firstName!:	string;
  lastName!:	string;
  email!:	string;
  birthDay: Date;
  registeredAt: Date;

  public constructor(
    Id: number, TeamId: number, FirstName: string,
    LastName: string, Email: string, BirthDay: Date,
    RegisteredAt: Date
  ) {
      this.id = Id;
      this.teamId = TeamId;
      this.firstName = FirstName;
      this.lastName = LastName;
      this.email = Email;
      this.birthDay = BirthDay;
      this.registeredAt = RegisteredAt;
  }
}
