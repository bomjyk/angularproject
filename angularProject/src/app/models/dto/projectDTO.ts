import {Team} from "../team";

export class ProjectDTO {
  id: number;
  authorId: number;
  teamId: number;
  name!:	string;
  description!:	string;
  deadline:	Date;
  createdAt: Date;

  public constructor(
    Id: number, AuthorId: number, TeamId: number,
    Name: string, Description: string, Deadline: Date,
    CreatedAt: Date
  ) {
    this.id = Id;
    this.authorId = AuthorId;
    this.teamId = TeamId;
    this.name = Name;
    this.description = Description;
    this.deadline = Deadline;
    this.createdAt = CreatedAt;
  }
}
