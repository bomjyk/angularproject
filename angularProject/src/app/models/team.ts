export class Team {
  public id: number;
  public name: string;
  public createdAt: Date;

  public constructor(id: number, name: string, createdAt: Date) {
    this.id = id;
    this.name = name;
    this.createdAt = createdAt;
  }
}
