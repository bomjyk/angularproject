import {User} from "./User";
import {Project} from "./project";

export class MyTask {
  Id: number;
  Name: string;
  Description: string;
  User: User;
  Project: Project;
  CreatedAt: Date;
  FinishedAt!: Date;
  State: TaskState;

  public constructor(
    id:number,
    name: string,
    description: string,
    user: User,
    project: Project,
    createdAt: Date,
    finishedAt: Date | null = null,
    state: TaskState) {
    this.Id = id;
    this.Name = name;
    this.Description = description;
    this.User = user;
    this.Project = project;
    this.CreatedAt = createdAt;
    if(finishedAt) {
      this.FinishedAt = finishedAt;
    }
    this.State = state;
  }
}

export enum TaskState {
  'finished' = 0,
  'inProgress' = 1,
  'abandon' = 2,
  'stopped' = 3
}
