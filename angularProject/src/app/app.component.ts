import { Component } from '@angular/core';
import {ProjectsListComponent} from "./projects-module/projects-list/projects-list.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularProject';
}
