import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import {Observable, of, throwError} from "rxjs";
import {catchError, map, retry} from "rxjs/operators";
import {Project} from "../models/project";
import {ProjectDTO} from "../models/dto/projectDTO";
import {Team} from "../models/team";
import {User} from "../models/User";
import {TeamsService} from "../teams/teams.service";
import {UserService} from "../users/user.service";

const isTest: boolean = false;
const API_Project_URI: string = 'https://localhost:5001/api/Projects';
const API_User_URI: string = 'https://localhost:5001/api/Users';
const API_Team_URI: string = 'https://localhost:5001/api/Teams';
const getId_URI: string = '/id?id=';

@Injectable({
  providedIn: 'root'
})
export class ProjectServiceService {
  private API_URI : string = "https://localhost:5001/api/Projects";
  localProjects: Project[] = [];
  public GetAllProjects() : Observable<Project[]> {
    console.log(this.API_URI);
    if(isTest == true) {
      return of(this.localProjects)
    } else {
      let projectsDTO : Observable<ProjectDTO[]> = this.http.get<ProjectDTO[]>(API_Project_URI);
      let teams : Team[];
      this.teamsService.getAllTeams().subscribe(t => teams = t);
      let users : User[];
      this.userService.getAllUsers().subscribe(u => users = u);
      let projects : Observable<Project[]>;
      projects = projectsDTO.pipe(
        map(p => {
          return p.map(pr => {
            return new Project(pr.id,
              <User>users.find(u => u.Id == pr.authorId),
              <Team>teams.find(t => t.id == pr.teamId),
              pr.name,
              pr.description,
              pr.deadline,
              pr.createdAt);
          })
        })
      );
      return projects;
    }
  }
  public GetProject(id: number) : Observable<Project> {
    return isTest == true ? of(this.localProjects[id-1]) :
      this.http.get<Project>(this.API_URI + 'id?=' + id);
  }
  public UpdateProject(id: number, project: Project) : void {
    if(isTest == true) {
      project.id = id;
      project.deadline = new Date(project.deadline);
      this.localProjects[id-1] = project;
    } else {
      throw Error;
    }
  }
  public CreateProject(project: Project) : void {
    if(isTest == true) {
      project.id = this.localProjects.length + 1;
      project.createdAt = new Date();
      project.deadline = new Date(project.deadline);
      this.localProjects.push(project);
    } else {
      throw Error;
    }
  }
  constructor(private  http: HttpClient,
              private teamsService: TeamsService,
              private userService: UserService) { }
}
