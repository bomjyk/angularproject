import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { HttpClientModule} from "@angular/common/http";
import { BrowserModule} from "@angular/platform-browser";
import { ProjectServiceService } from "./project-service.service";
import { RouterModule, Routes } from "@angular/router";
import { ProjectsEditComponent } from './projects-edit/projects-edit.component';
import {FormsModule} from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { ProjectCreateComponent } from './project-create/project-create.component';
import {UkrainianDatePipe} from "../pipes/ukrainianDatePipe";
import {TaskHighlightDirective} from "../directives/task-highlight.directive";
import {ProjectFormDeactivateGuard} from "../guards/project-form-deactivate.guard";
import {CustomPipesModuleModule} from "../pipes/cutom-pipes-module/cutom-pipes-module.module";
import {TaskHighlightModule} from "../directives/task-highlight/task-highlight.module";
import {UsersModule} from "../users/users.module";
import {TeamsModule} from "../teams/teams.module";
import {ProjectEditDeactivateGuard} from "../guards/project-edit-deactivate.guard";

const routes : Routes = [
  { path: 'project/list', component: ProjectsListComponent },
  { path: 'project/edit/:id', component: ProjectsEditComponent,
  canDeactivate: [ProjectEditDeactivateGuard]},
  { path: 'project/create', component: ProjectCreateComponent,
    canDeactivate: [ProjectFormDeactivateGuard]}
];

@NgModule({

  declarations: [
    ProjectsListComponent,
    ProjectsEditComponent,
    ProjectCreateComponent,

  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    CustomPipesModuleModule,
    TaskHighlightModule,
    UsersModule,
    TeamsModule
  ],
  exports: [
    ProjectsListComponent,
    RouterModule
  ],
  providers: [
    ProjectServiceService,
  ]
})
export class ProjectsModuleModule { }
