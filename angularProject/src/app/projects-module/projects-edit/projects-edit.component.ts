import {Component, OnInit, ViewChild} from '@angular/core';
import { Input } from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import { Project } from 'src/app/models/project';
import {ProjectServiceService} from "../project-service.service";
import {FormBuilder, FormControl, FormGroup, FormsModule, NgForm, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {TeamsService} from "../../teams/teams.service";
import {UserService} from "../../users/user.service";
import {User} from "../../models/User";
import {Team} from "../../models/team";


@Component({
  selector: 'app-projects-edit',
  templateUrl: './projects-edit.component.html',
  styleUrls: ['./projects-edit.component.css']
})
export class ProjectsEditComponent implements OnInit {
  @ViewChild("userForm")
  private userForm : NgForm | null = null;
  formSubmited: boolean = false;
  form!: FormGroup;
  project!: Project;
  projectForm!: FormGroup;
  users!: User[];
  teams!: Team[];
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _service: ProjectServiceService,
    private _teamService: TeamsService,
    private _userService: UserService,
    private _forms: FormsModule
  ) {
    _teamService.getAllTeams().subscribe(t => {
      this.teams = t;
    });
    _userService.getAllUsers().subscribe(u => {
      this.users  = u;
    })
  }

  id!: number;

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.id = +params['id'];
      this.getProject(this.id);
    });
  }

  private getProject(id: number) {
    this._service.GetProject(id).subscribe(pr => {
      this.project = pr;
      this.projectForm = new FormGroup({
        name: new FormControl(this.project.name, [Validators.required,
          Validators.minLength(2)]),
        description: new FormControl(this.project.description, [Validators.required,
        Validators.maxLength(60)]),
        team: new FormControl(this.project.team, [Validators.required]),
        author: new FormControl(this.project.author, [Validators.required]),
        deadline: new FormControl(this.project.deadline, [Validators.required]),
        createdAt: new FormControl(this.project.createdAt)
      });
    });
  }
  onSubmit() {
    this.updateProject();
  }
  updateProject() {
    this.formSubmited = true;
    this._service.UpdateProject(this.id, this.projectForm.value);
    this._router.navigate(['../../list'], {relativeTo: this._route})
  }

  public hasUnsavedData() : boolean {
    return <boolean>this.userForm?.dirty;
  }
}
