import {Component, OnInit, ViewChild} from '@angular/core';
import {Project} from "../../models/project";
import {FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {ProjectServiceService} from "../project-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../users/user.service";
import {TeamsService} from "../../teams/teams.service";
import {Team} from "../../models/team";
import {User} from "../../models/User";

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {
  @ViewChild("userForm")
  private userForm : NgForm | null = null;
  formSubmited: boolean = false;
  project!: Project;
  projectForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required,
    Validators.minLength(2)]),
    description: new FormControl('',
      [Validators.required, Validators.maxLength(60)]),
    team: new FormControl(null, [Validators.required]),
    author: new FormControl(null, [Validators.required]),
    deadline: new FormControl(null, [Validators.required])
  });
  teams!: Team[];
  users!: User[];
  constructor(
    private _service: ProjectServiceService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _userService: UserService,
    private _teamService: TeamsService
  ) {
    _userService.getAllUsers().subscribe(u => {
      this.users = u;
    });
    _teamService.getAllTeams().subscribe(t => {
      this.teams = t;
    })
  }

  ngOnInit(): void {

  }

  onSubmit() {
    this.formSubmited = true;
    this._service.CreateProject(this.projectForm.value);
    this._router.navigate(['../list'], { relativeTo: this._route });
  }

  public hasUnsavedData() : boolean {
    return <boolean>this.userForm?.dirty;
  }
}
