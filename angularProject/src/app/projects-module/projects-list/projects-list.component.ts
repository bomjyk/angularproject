import { Component, OnInit } from '@angular/core';
import { Project } from '../../models/project';
import {ProjectServiceService} from "../project-service.service";
import {Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {UkrainianDatePipe} from "../../pipes/ukrainianDatePipe";

const API_URI : string = "https://localhost:5001/api/Projects";

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})

export class ProjectsListComponent implements OnInit {
  projects: Project[] = [];
  constructor(private service: ProjectServiceService,
              private _route: ActivatedRoute,
              private _router: Router) { }

  ngOnInit(): void {
    console.log("Refreshed");
    this.service.GetAllProjects().subscribe( {
      next: pr => { this.projects = pr; },
      error: (er) => console.log(er)
      }
    );
  }

  showEditForm(id: number) {
    this._router.navigate(['project/edit', id]);
  }
  createNewProject() {
    this._router.navigate(['project/create']);
  }
}

