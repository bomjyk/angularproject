import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import {UserService} from "./user.service";
import {UkrainianDatePipe} from "../pipes/ukrainianDatePipe";
import {CustomPipesModuleModule} from "../pipes/cutom-pipes-module/cutom-pipes-module.module";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {TeamsModule} from "../teams/teams.module";
import {UserEditDeactivateGuard} from "../guards/users/user-edit-deactivate.guard";
import {UserCreateDeactivateGuard} from "../guards/users/user-create-deactivate.guard";

const routes: Routes = [
  {path:'user/list', component: UserListComponent},
  {path:'user/edit/:id', component: UserEditComponent,
  canDeactivate: [UserEditDeactivateGuard]},
  {path:'user/create', component: UserCreateComponent,
  canDeactivate: [UserCreateDeactivateGuard]}
];


@NgModule({
  declarations: [
    UserCreateComponent,
    UserListComponent,
    UserEditComponent,

  ],
  imports: [
    CommonModule,
    CustomPipesModuleModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    TeamsModule
  ],
  providers: [
    UserService,

  ]
})
export class UsersModule { }
