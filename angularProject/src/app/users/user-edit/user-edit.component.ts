import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, FormsModule, NgForm, Validators} from "@angular/forms";
import {MyTask} from "../../models/task";
import {ActivatedRoute, Router} from "@angular/router";
import {TasksService} from "../../tasks/tasks.service";
import {UserService} from "../user.service";
import {User} from "../../models/User";
import {Team} from "../../models/team";
import {TeamsService} from "../../teams/teams.service";

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  teams!: Team[];
  @ViewChild("saveForm")
  private saveForm : NgForm | null = null;
  formSubmited: boolean = false;
  form!: FormGroup;
  user!: User;
  userForm!: FormGroup;
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _service: UserService,
    private _teamService: TeamsService,
    private _forms: FormsModule
  ) {
    _teamService.getAllTeams().subscribe(t => {
      this.teams = t;
    })
  }

  id!: number;

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.id = +params['id'];
      this.getUser(this.id);
    });
  }

  private getUser(id: number) {
    this._service.getUserById(id).subscribe(pr => {
      this.user = pr;
      this.userForm = new FormGroup({
        Name: new FormControl(this.user.Name, [Validators.required, Validators.minLength(4)]),
        Email: new FormControl(this.user.Email, [Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
        Team: new FormControl(this.user.Team, [Validators.required]),
        Birthday: new FormControl(this.user.Birthday, [Validators.required])
      });
    });
  }
  onSubmit() {
    this.updateProject();
  }
  updateProject() {
    console.log(this.userForm.value);
    this.formSubmited = true;
    this._service.updateUser(this.id, this.userForm.value);
    this._router.navigate(['../../list'], {relativeTo: this._route})
  }

  public hasUnsavedData() : boolean {
    return <boolean>this.saveForm?.dirty;
  }
}
