import { Injectable } from '@angular/core';
import {User} from "../models/User";
import {Observable, of} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {map, mergeMap} from "rxjs/operators";
import {Project} from "../models/project";
import {UserDTO} from "../models/dto/userDTO";
import {ProjectDTO} from "../models/dto/projectDTO";
import {TeamsService} from "../teams/teams.service";
import {Team} from "../models/team";

const isDev: boolean = false;
const API_Project_URI: string = 'https://localhost:5001/api/Projects';
const API_User_URI: string = 'https://localhost:5001/api/Users';
const API_Team_URI: string = 'https://localhost:5001/api/Teams';
const getId_URI: string = '/id?id=';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[] = [];

  constructor(private http: HttpClient,
              private userService: UserService,
              private teamService: TeamsService) { }

  getAllUsers() : Observable<User[]> {
    if(isDev) {
      return of(this.users);
    } else {
      return of(this.users);
    }

  }

  getUserById(id: number) : Observable<User> {
    return of(this.users[id-1]);
  }

  updateUser(id:number, user: User) : void {
    user.Id = id;
    user.Birthday = new Date(user.Birthday);
    user.RegisteredAt = this.users[id-1].RegisteredAt;
    this.users[id-1] = user;
  }

  createUser(user:User) : void {
    user.Id = this.users.length + 1;
    user.Birthday = new Date(user.Birthday);
    user.RegisteredAt = new Date();
    this.users.push(user);
  }

}
