import {Component, OnInit, ViewChild} from '@angular/core';
import {EmailValidator, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {MyTask} from "../../models/task";
import {TasksService} from "../../tasks/tasks.service";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../models/User";
import {UserService} from "../user.service";
import {TeamsService} from "../../teams/teams.service";
import {Team} from "../../models/team";

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  teams!: Team[];
  @ViewChild("saveForm")
  private saveForm : NgForm | null = null;
  formSubmited: boolean = false;
  user!: User;
  userForm: FormGroup = new FormGroup({
    Name: new FormControl(''),
    Email: new FormControl('', [Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    Team: new FormControl(null, [Validators.required]),
    Birthday: new FormControl(this.teams, [Validators.required])
  });
  constructor(
    private _service: UserService,
    private _teamsService: TeamsService,
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    _teamsService.getAllTeams().subscribe(t =>
    {
      this.teams = t;
    })
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.userForm.value);
    this.formSubmited = true;
    this._service.createUser(this.userForm.value);
    this._router.navigate(['../list'], { relativeTo: this._route });
  }

  public hasUnsavedData() : boolean {
    return <boolean>this.saveForm?.dirty;
  }

  get Email() {
    return this.userForm.get('Email');
  }
}
