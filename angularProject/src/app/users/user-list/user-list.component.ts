import { Component, OnInit } from '@angular/core';
import {MyTask} from "../../models/task";
import {TasksService} from "../../tasks/tasks.service";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../models/User";
import {UserService} from "../user.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users!: User[];
  constructor(
    private _service: UserService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._service.getAllUsers().subscribe(us => {
      this.users = us;

    })
  }

  showEditForm(id: number) {
    this._router.navigate(['user/edit', id]);
  }

  createNewUser() {
    this._router.navigate(['user/create']);
  }
}
