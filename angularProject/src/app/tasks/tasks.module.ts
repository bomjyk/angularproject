import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksListComponent } from './tasks-list/tasks-list.component';
import { TasksCreateComponent } from './tasks-create/tasks-create.component';
import { TasksEditComponent } from './tasks-edit/tasks-edit.component';
import {CustomPipesModuleModule} from "../pipes/cutom-pipes-module/cutom-pipes-module.module";
import {RouterModule, Routes} from "@angular/router";
import {ReactiveFormsModule} from "@angular/forms";
import {TaskHighlightDirective} from "../directives/task-highlight.directive";
import {TaskHighlightModule} from "../directives/task-highlight/task-highlight.module";
import {ProjectsModuleModule} from "../projects-module/projects-module.module";
import {UsersModule} from "../users/users.module";
import {TaskEditGuardGuard} from "../guards/tasks/task-edit-guard.guard";
import {TaskCreateGuardGuard} from "../guards/tasks/task-create-guard.guard";

const routes: Routes = [
  {path: 'task/list', component: TasksListComponent},
  {path: 'task/edit/:id', component: TasksEditComponent,
  canDeactivate: [TaskEditGuardGuard]},
  {path: 'task/create', component:TasksCreateComponent,
  canDeactivate: [TaskCreateGuardGuard]}
]

@NgModule({
  declarations: [
    TasksListComponent,
    TasksCreateComponent,
    TasksEditComponent
  ],
  imports: [
    CommonModule,
    CustomPipesModuleModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    TaskHighlightModule,
    ProjectsModuleModule,
    UsersModule
  ]
})
export class TasksModule { }
