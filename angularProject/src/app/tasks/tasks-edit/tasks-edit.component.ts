import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, FormsModule, NgForm, Validators} from "@angular/forms";
import {Team} from "../../models/team";
import {ActivatedRoute, Router} from "@angular/router";
import {TeamsService} from "../../teams/teams.service";
import {MyTask, TaskState} from "../../models/task";
import {TasksService} from "../tasks.service";
import {User} from "../../models/User";
import {Project} from "../../models/project";
import {UserService} from "../../users/user.service";
import {ProjectServiceService} from "../../projects-module/project-service.service";

@Component({
  selector: 'app-tasks-edit',
  templateUrl: './tasks-edit.component.html',
  styleUrls: ['./tasks-edit.component.css']
})
export class TasksEditComponent implements OnInit {
  @ViewChild("saveForm")
  private saveForm : NgForm | null = null;
  formSubmited: boolean = false;
  form!: FormGroup;
  task!: MyTask;
  taskForm!: FormGroup;
  users!: User[];
  projects!: Project[];
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _service: TasksService,
    private _forms: FormsModule,
    private _userService: UserService,
    private _projectService: ProjectServiceService
  ) {
    _userService.getAllUsers().subscribe(u => {
      this.users = u;
    });
    _projectService.GetAllProjects().subscribe(p => {
      this.projects = p;
    });
  }

  id!: number;

  ngOnInit(): void {
    this._route.params.subscribe(params => {
      this.id = +params['id'];
      this.getTask(this.id);
    });
  }

  private getTask(id: number) {
    this._service.getTaskById(id).subscribe(pr => {
      this.task = pr;
      this.taskForm = new FormGroup({
        Name: new FormControl(this.task.Name, [Validators.required,
          Validators.minLength(2)]),
        Description: new FormControl(this.task.Description, [Validators.required,
        Validators.maxLength(60)]),
        User: new FormControl(this.task.User, [Validators.required]),
        Project: new FormControl(this.task.Project, [Validators.required]),
        State: new FormControl(this.task.State, [Validators.required,
        Validators.min(0),
        Validators.max(Object.keys(TaskState).length)]),
        FinishedAt: new FormControl(this.task.FinishedAt)
      });
    });
  }
  onSubmit() {
    this.updateProject();
  }
  updateProject() {
    this.formSubmited = true;
    this._service.updateTask(this.id, this.taskForm.value);
    this._router.navigate(['../../list'], {relativeTo: this._route})
  }
  public hasUnsavedData() : boolean {
    return <boolean>this.saveForm?.dirty;
  }
}
