import { Injectable } from '@angular/core';
import {Observable, of} from "rxjs";
import {MyTask} from "../models/task";

const isDev: boolean = true;

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  localTasks: MyTask[] = [];

  constructor() { }

  getTaskById(id: number) : Observable<MyTask> {
    return of(this.localTasks[id-1]);
  }

  getAllTasks() : Observable<MyTask[]> {
    return of(this.localTasks);
  }

  updateTask(id: number, task: MyTask) : void {
    task.Id = id;
    if(task.FinishedAt != null) task.FinishedAt = new Date(task.FinishedAt);
    task.CreatedAt = this.localTasks[id-1].CreatedAt;
    this.localTasks[id-1] = task;
  }

  createTask(task: MyTask) : void {
    task.Id = this.localTasks.length+1;
    if(task.FinishedAt != null) task.FinishedAt = new Date(task.FinishedAt);
    task.CreatedAt = new Date();
    this.localTasks.push(task);
  }

}
