import { Component, OnInit } from '@angular/core';
import {TasksService} from "../tasks.service";
import {MyTask, TaskState} from "../../models/task";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.css']
})
export class TasksListComponent implements OnInit {
  tasks!: MyTask[];
  constructor(
    private _service: TasksService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._service.getAllTasks().subscribe(ts => {
      this.tasks = ts;
    })
  }

  showEditForm(id: number) {
    this._router.navigate(['task/edit', id]);
  }

  createNewTask() {
    this._router.navigate(['task/create']);
  }

  taskState(task: MyTask) {
    return TaskState[task.State];
  }
}
