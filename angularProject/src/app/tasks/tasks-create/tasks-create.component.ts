import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Team} from "../../models/team";
import {TeamsService} from "../../teams/teams.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MyTask, TaskState} from "../../models/task";
import {TasksService} from "../tasks.service";
import {User} from "../../models/User";
import {Project} from "../../models/project";
import {UserService} from "../../users/user.service";
import {ProjectServiceService} from "../../projects-module/project-service.service";

@Component({
  selector: 'app-tasks-create',
  templateUrl: './tasks-create.component.html',
  styleUrls: ['./tasks-create.component.css']
})
export class TasksCreateComponent implements OnInit {

  @ViewChild("saveForm")
  private saveForm : NgForm | null = null;
  formSubmited: boolean = false;
  task!: MyTask;
  users!: User[];
  projects!: Project[];
  taskForm: FormGroup = new FormGroup({
    Name: new FormControl('', [Validators.required,
    Validators.minLength(2)]),
    Description: new FormControl('', [Validators.required,
    Validators.maxLength(60)]),
    User: new FormControl(null, [Validators.required]),
    Project: new FormControl(null, [Validators.required]),
    State: new FormControl(0, [Validators.required,
    Validators.min(0),
    Validators.max(Object.keys(TaskState).length)]),
    FinishedAt: new FormControl(null)
  });
  constructor(
    private _service: TasksService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _userService: UserService,
    private _projectService: ProjectServiceService
  ) {
    _userService.getAllUsers().subscribe(u => {
      this.users = u;
    });
    _projectService.GetAllProjects().subscribe(p => {
      this.projects = p;
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.formSubmited = true;
    this._service.createTask(this.taskForm.value);
    this._router.navigate(['../list'], { relativeTo: this._route });
  }

  public hasUnsavedData() : boolean {
    return <boolean>this.saveForm?.dirty;
  }

  getTaskStateNumber() {
    return Object.keys(TaskState).length;
  }

}
