import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TaskHighlightDirective} from "../task-highlight.directive";



@NgModule({
  declarations: [
    TaskHighlightDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TaskHighlightDirective
  ]
})
export class TaskHighlightModule { }
