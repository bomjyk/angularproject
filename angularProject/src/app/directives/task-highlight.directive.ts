import {Directive, ElementRef, HostBinding, Inject, Renderer2} from '@angular/core';
import {DOCUMENT} from "@angular/common";

@Directive({
  selector: '[appTaskHighlight]'
})
export class TaskHighlightDirective {

  @HostBinding('class')
  elementClass = 'green-circle';
  constructor(
    private _elementRef: ElementRef,
    private _renderer: Renderer2,
    @Inject(DOCUMENT) private document: any) {
    //_elementRef.nativeElement.addClass('green-circle');
    const child = document.createElement('i');
    child.style.cssText = "display:inline-block;border-radius:50%;margin-right:5px;width:10px;height:10px;background-color:green;";
    this._renderer.appendChild(this._elementRef.nativeElement, child);

  }
  ngOnInit() {

  }
}
